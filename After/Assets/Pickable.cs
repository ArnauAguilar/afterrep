﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Pickable : MonoBehaviour
{
    public GameObject Text;
    private GameObject insText;
    
    public bool picked = false;
    public bool pickable = false;
    public bool thrown = false;
    void Update()
    {   

        if (Vector3.Angle(transform.position - Camera.main.transform.position, Camera.main.transform.forward) < 15 && Vector3.Distance(transform.position, GameController.instance.player.actualPosition) < 3)
        {
            pickable = true;
        }
        else if (insText != null)
        {
            pickable = false;
            Destroy(insText);
        }
        if (picked && Input.GetKeyDown(KeyCode.R))
        {
            picked = false;
            transform.parent = null;
            GameController.instance.player.objectOnHand = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward.normalized * 1000);
            thrown = true;
        }
        if (picked)
        {
            Destroy(insText);
            return;
        }
        if (pickable && insText == null)
        {
            insText = Instantiate(Text, GameObject.Find("Canvas").transform);
            insText.GetComponent<TextMeshProUGUI>().SetText("E");
        }

        if (Input.GetKeyDown(KeyCode.E) && pickable && insText != null && !GameController.instance.player.objectOnHand)
        {
            transform.parent = Camera.main.transform;
            transform.localRotation = Quaternion.identity;
            transform.localPosition = new Vector3(-0.3900015f, -0.4270001f, 0.5820023f);
            GameController.instance.player.objectOnHand = true;
            picked = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (thrown)
        {
            thrown = false;
            GameController.instance.persequeSpot = transform.position;
        }
        if(collision.gameObject.tag == "drone")
        {
            collision.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<GuardBehaviour>().dead = true;
            collision.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            collision.gameObject.GetComponent<Rigidbody>().useGravity = true;
            collision.gameObject.GetComponent<Rigidbody>().AddForce(-collision.contacts[0].normal * 220);
        }
    }

}
