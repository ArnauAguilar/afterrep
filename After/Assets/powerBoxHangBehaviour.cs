﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerBoxHangBehaviour : MonoBehaviour
{
    Transform TransformToRotate;
    private float speed = 0;
    public float movementSpeed = 5;
    public float movementAmount = 100;
    public List<GameObject> sparks;
    public GameObject fance;
    // Start is called before the first frame update
    void Start()
    {
        TransformToRotate = transform.parent;
    }
    bool forceApl = false;
    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<Shotable>().shooted)
        {
            speed = Mathf.Sin(Time.time  * movementSpeed) * movementAmount;
            TransformToRotate.Rotate(new Vector3(1, 0, 0), Time.deltaTime * speed);
        }
        else
        {
            foreach (GameObject sp in sparks) sp.SetActive(false);
            GetComponent<Rigidbody>().isKinematic = false;
            fance.GetComponent<Rigidbody>().isKinematic = false;
            if (!forceApl) {
                fance.GetComponent<Rigidbody>().AddForce(-fance.transform.forward);
            }
        }
    }
}
