﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DronePathFollow : MonoBehaviour
{
    public List<GameObject> path;
    int current = 0;
    public float speed;
    void Update()
    {
        //if (current < 6) return;
        if(Vector3.Distance(transform.position, path[current].transform.position) > 1)
        {
            Vector3 direction = path[current].transform.position - transform.position;
            transform.position += direction.normalized * Time.deltaTime * speed;

            transform.forward = Vector3.Lerp(transform.position + transform.forward, transform.position + direction.normalized, Time.deltaTime*5) - transform.position;
        }
        else
        {
            current++;
        }

    }
}
