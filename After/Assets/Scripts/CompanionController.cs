﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class ExtensionMethods
{
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}


public class CompanionController : MonoBehaviour
{
    public enum Action
    {
        followPlayer,
        followPath,
        wait,
        evil
    }
    private bool stedy;
    public Action action;
    public Vector3 actualPosition;
    private float movementSpeed;
    public float speed;
    public FollowPath[] paths;
    public float walkSpeed, runSpeed, crouchSpeed;
    [HideInInspector]public float _stopDistance = 2;
    public float stopDistance = 2;
    public float evilStopDistance = 2;
    public float stopSpeed = 2;
    private FPCharacter player;
    public CharacterController _charController;
    public Animator anim;
    public GameObject leftHand;
    public GameObject RightHand;
    public GameObject GunPrefab;
    private GameObject gun;

    public float life = 100;

    // Start is called before the first frame update
    void Start()
    {
        anim = transform.GetComponentInChildren<Animator>();
        _charController = GetComponent<CharacterController>();
        player = GameObject.Find("Player").GetComponent<FPCharacter>();
        actualPosition = transform.position;
        movementSpeed = 0;
        gun = Instantiate(GunPrefab, RightHand.transform);
    }
    private bool moving;
    /// <summary>
    /// Sets movement Speed
    /// </summary>
    /// <param name="targget"></param>
    /// <param name="state"></param>
    void move(Vector3 targget, FPCharacter.state state)
    {
        if (state == FPCharacter.state.crouched)
        {
            anim.SetBool("Crouched", true);
            if (Vector3.Distance(actualPosition, targget) < _stopDistance)
            {
                movementSpeed -= Time.deltaTime * stopSpeed;
                if (movementSpeed < 0) movementSpeed = 0;
                moving = false;
            }
            else
            {
                moving = true;
                movementSpeed = (movementSpeed < crouchSpeed) ? movementSpeed + Time.deltaTime * stopSpeed : movementSpeed - Time.deltaTime * stopSpeed;
            }
        }
        else
        {
            if (Vector3.Distance(actualPosition, targget) < _stopDistance)
            {
                moving = false;
                movementSpeed -= Time.deltaTime * stopSpeed;
                if (movementSpeed < 0) movementSpeed = 0;
            }
            else if (state == FPCharacter.state.runing)
            {
                moving = true;
                movementSpeed = (movementSpeed < runSpeed) ? movementSpeed + Time.deltaTime * stopSpeed : runSpeed;
            }
            else
            {
                moving = true;
                movementSpeed = (movementSpeed < walkSpeed) ? movementSpeed + Time.deltaTime * stopSpeed : movementSpeed - Time.deltaTime * stopSpeed;
            }
        }
    }

    public int pathToFollow;
    Vector3 target;
    Vector3 realTarget;
    float changeTargetCounter;
    float gunRepositionCounter;

    IEnumerator setNextLocation(float t)
    {
        yield return new WaitForSecondsRealtime(t);
        paths[pathToFollow].current = Random.Range(0, paths[pathToFollow].points.Length);
        changeTargetCounter = 0;
        stopTime = 0;
        stedy = false;
    }
    private float stopTime = 0;
    public FPCharacter.state animState = FPCharacter.state.walking;
    void Update()
    {
        Vector3 movement = Vector3.zero;
        anim.SetBool("Crouched", false);
        if (life > 0)
        {

            switch (action)
            {
                case Action.followPlayer:
                    _stopDistance = stopDistance;
                    move(player.actualPosition, player.State);
                    target = player.actualPosition;
                    break;
                case Action.followPath:
                    _stopDistance = stopDistance;

                    if (!paths[pathToFollow].finished())
                    {
                        if (Vector3.Distance(actualPosition, paths[pathToFollow].getNextPoint().transform.position) < _stopDistance)
                        {
                            paths[pathToFollow].goToNext();
                            changeTargetCounter = 0;
                        }
                        else move(paths[pathToFollow].getNextPoint().transform.position, animState);

                        realTarget = paths[pathToFollow].getNextPoint().transform.position;
                        target = Vector3.Lerp(target, realTarget, changeTargetCounter / 10);
                        changeTargetCounter += Time.deltaTime;
                    }
                    else
                    {
                        Debug.Log("Path FInished");
                        target = actualPosition;
                    }
                    break;
                case Action.evil:
                    _stopDistance = evilStopDistance;

                    Vector3 nextPoint = paths[pathToFollow].getNextPoint().transform.position;
                    Debug.DrawLine(nextPoint, actualPosition);

                    if (Vector3.Distance(actualPosition, nextPoint) < _stopDistance)
                    {
                        if (!stedy) StartCoroutine(setNextLocation(7));
                        stedy = true;
                        target = Vector3.Lerp(nextPoint, actualPosition, stopTime);
                        move(target, FPCharacter.state.walking);
                        stopTime += Time.deltaTime;
                    }
                    else
                    {
                        move(nextPoint, FPCharacter.state.walking);

                        realTarget = nextPoint;
                        target = Vector3.Lerp(target, realTarget, changeTargetCounter / 10);
                        changeTargetCounter += Time.deltaTime;
                    }

                    break;
                case Action.wait:
                    _stopDistance = stopDistance;
                    movementSpeed = 0;
                    target = actualPosition;
                    movement = Vector3.zero;
                    move(actualPosition, animState);
                    break;
            }

            movement = target - actualPosition;
            movement.Normalize();
            movement *= movementSpeed * speed;
            movement = Vector3.ClampMagnitude(movement, movementSpeed);
            movement.y = GetComponent<Rigidbody>().velocity.y;
            movement *= Time.deltaTime;
            movement = transform.TransformDirection(movement);
            _charController.Move(movement);

            if (!anim.GetBool("Crouched")) anim.SetFloat("Speed", movementSpeed / runSpeed);
            else anim.SetFloat("Speed", movementSpeed.Remap(0, crouchSpeed, 0, 1));


            Vector3 orientation = new Vector3(movement.normalized.x, 0, movement.normalized.z);
            transform.GetChild(0).forward = orientation == Vector3.zero ? new Vector3(player.actualPosition.x, 0, player.actualPosition.z) - new Vector3(actualPosition.x, 0, actualPosition.z) : orientation;
            Debug.DrawRay(actualPosition, actualPosition + orientation, Color.red);


            float handDistance01 = Vector3.Distance(RightHand.transform.position, leftHand.transform.position);
            handDistance01 = handDistance01.Remap(0.4f, 0.9f, 0, 1);
            handDistance01 = Mathf.Clamp01(handDistance01);

            if (handDistance01 < 0.2f)
            {
                if (player.State == FPCharacter.state.crouched) gun.transform.forward = Vector3.Lerp(gun.transform.forward, leftHand.transform.position - RightHand.transform.position, gunRepositionCounter * 10);
                else gun.transform.forward = Vector3.Lerp(gun.transform.forward, leftHand.transform.position - RightHand.transform.position, gunRepositionCounter);
                gunRepositionCounter += Time.deltaTime;
            }
            else
            {
                gun.transform.forward = RightHand.transform.GetChild(1).position - RightHand.transform.position;
                gunRepositionCounter = 0;
            }

        }
        else _charController.Move(movement);
        if (action == Action.evil && !dead) if (life <= 0)
        {
                dead = true;
                anim.SetBool("Dead", true);
                anim.SetTrigger("DeadTrigger");
        }


        actualPosition = transform.position;
    }
    public bool dead = false;


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
       //BUllets collisions not done here because character controller, check gun script for it

        //////////
        #region pushStuff
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body == null || body.isKinematic)
        {
            return;
        }
        if (hit.moveDirection.y < -0.3)
        {
            return;
        }

        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        body.velocity = pushDir * 2;
        #endregion
        //////////
    }

}
