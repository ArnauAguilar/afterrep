﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public GameObject hitParticles;

    private void Update()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.name);
        GameObject particles = Instantiate(hitParticles);
        particles.transform.position = transform.position;
        particles.transform.forward = collision.contacts[0].normal;
        if (collision.gameObject.tag == "Movable")
        {
            Destroy(particles, 0.5f);
        }
        else Destroy(particles, 30f);
        Destroy(this.gameObject);
 
    }
}
