﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPath", menuName = "ScriptableObjects/Path", order = 1)]

public class FollowPath : ScriptableObject
{
    public string[] points;
    public int current;

    public bool finished()
    {
        return current + 1> points.Length;
    }
    public GameObject getNextPoint()
    {
        if (current >= points.Length)
        {
            current++;
            return null;
        }
     
        return GameObject.Find(points[current]);
    }
    public void goToNext()
    {
        current += 1;
    }

}
