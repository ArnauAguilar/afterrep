﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBehaviour : MonoBehaviour
{
    public Vector2 atenuation;
    public Vector3 pointingPos;

    Vector3 originalPos;
    public float resetTimer = 0;
    FPCharacter player;
    public GameObject marker;
    private GameObject markerInstance;
    public GameObject hitParticles;
    public GameObject bullet;
    public float shootForce;
    public AudioClip sound;
    public GameObject musleFlash;
    public bool Aim = false;
    public bool UnAim = false;
    public bool shoot = false;
        
    // Start is called before the first frame update
    void Start()
    {
        originalPos = transform.localPosition;
        if(GameObject.Find("Player"))
        player = GameObject.Find("Player").GetComponent<FPCharacter>();
    }
    public bool aiming;
    // Update is called once per frame
    RaycastHit hit;
    void Update()
    {

        if (Input.GetMouseButtonDown(1) || Aim)//aim
        {
            markerInstance = Instantiate(marker, GameObject.Find("Canvas").transform);
            aiming = true;
            resetTimer = 0;
        }
        if ((markerInstance != null && Input.GetMouseButtonUp(1)) || UnAim)//unAim
        {
            Destroy(markerInstance);
            aiming = false;
            if (GameObject.Find("Player").GetComponent<FPCharacter>().allowMovement) resetTimer = 0;
        }

        if (aiming)//DuringAiming
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, originalPos + pointingPos, Mathf.Clamp01(resetTimer));
            resetTimer += Time.deltaTime;
        }
        else //non Aiming
        {
            if (GameObject.Find("Player").GetComponent<FPCharacter>().allowMovement)
            {
                if (player.deltaX != 0 || player.deltaZ != 0)
                {
                    transform.localPosition = originalPos + new Vector3(-Mathf.Sin(Time.time * player.CameraWalkEffectSpeed.x) * player.CameraWalkEffect.x / atenuation.x, -Mathf.Cos(Time.time * player.CameraWalkEffectSpeed.y) * player.CameraWalkEffect.y / atenuation.y, 0);
                    resetTimer = 0;
                }
                else
                {
                    transform.localPosition = Vector3.Lerp(transform.localPosition, originalPos, Mathf.Clamp01(resetTimer));
                    resetTimer += Time.deltaTime;
                }
            }
            else
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, originalPos, Mathf.Clamp01(resetTimer));
                resetTimer += Time.deltaTime;
            }

        }
        int layerMask = 1 << 10;
        layerMask = ~layerMask;

        if (Input.GetMouseButtonDown(0) || shoot)
        {
            GetComponent<AudioSource>().clip = sound;
            GetComponent<AudioSource>().Play();
            GameObject IBullet = Instantiate(bullet);
            IBullet.transform.position = player.actualPosition + Vector3.up * 0.75f;
            IBullet.GetComponent<Rigidbody>().velocity = Camera.main.transform.forward * shootForce;
            IBullet.GetComponent<BulletBehaviour>().hitParticles = hitParticles;
            GameObject muzzleFlash = Instantiate(musleFlash);
            muzzleFlash.transform.parent = transform;
            muzzleFlash.transform.localPosition = new Vector3(0, 0.065f,0.146f);
            Destroy(muzzleFlash, 1);

            if (Physics.Raycast(player.actualPosition, Camera.main.transform.forward, out hit, Mathf.Infinity, layerMask))
            {
                if (hit.transform.gameObject.tag == "Movable")
                {
                    CompanionController c = hit.transform.GetComponent<CompanionController>();
                    if (c) c.life -= 10;
                }
            }
            shoot = false;
        }

    }
}
