﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardBehaviour : MonoBehaviour
{

    public float RotationAmount;
    public float RotationSpeed;
    public bool detected;
    private bool restart;
    public float movementDpeed;
    public Vector3 detectedPos;
    public bool nonFlying = false;
    public bool dead = false;
    void Update()
    {
        if (dead)
        {
            transform.root.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            transform.root.gameObject.GetComponent<Rigidbody>().useGravity = true;
            this.enabled = false;
            gameObject.SetActive(false);
            gameObject.transform.parent.GetChild(1).gameObject.SetActive(false);
            gameObject.transform.root.GetChild(1).gameObject.SetActive(true);    
        }
        else
        {
            transform.root.Rotate(Vector3.up, RotationAmount * Time.deltaTime * RotationSpeed* Mathf.Sign(Mathf.Sin(Time.time * RotationSpeed)));
            if (detected)
            {
                if (!restart) { 
                    StartCoroutine(GameController.instance.ChangeScene(1, 2));
                    restart = true;
                }
                Vector3 targget;
                if (!nonFlying) targget = GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition + ((transform.root.position - GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition).normalized * 1.5f);
                else
                {
                    targget = GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition + ((transform.root.position - GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition).normalized * 1.5f);
                    targget.y = transform.root.position.y;
                }
                transform.root.position = Vector3.MoveTowards(transform.root.position, targget , movementDpeed * Time.deltaTime);

                if (!nonFlying) transform.root.forward = GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition - transform.root.position + Vector3.up * 0.7f;
                else
                {
                    Vector3 f = GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition - transform.root.position;
                    f.y = 0;
                    transform.root.forward = f;
                }
                //stop movement
                GameObject.Find("Player").GetComponent<FPCharacter>().allowMovement = false;
                Camera.main.GetComponent<FPMouseLook>().enabled = false;
                GameObject.Find("Player").GetComponent<FPMouseLook>().enabled = false;

                if (!nonFlying) Camera.main.transform.forward = Vector3.RotateTowards(Camera.main.transform.forward, detectedPos - (GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition - Vector3.up * -0.5f), 3 * Time.deltaTime, 0);
                else Camera.main.transform.forward = Vector3.RotateTowards(Camera.main.transform.forward, detectedPos - (GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition - Vector3.up * 0.5f), 3 * Time.deltaTime, 0);
            }
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            int layerMask = 1 << 12; 
            layerMask = ~layerMask;
            RaycastHit hit;
            if (Physics.Raycast(transform.root.position, GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition - transform.root.position, out hit, Mathf.Infinity, layerMask))
            {
                Debug.DrawLine(transform.root.position, transform.root.position +((GameObject.Find("Player").GetComponent<FPCharacter>().actualPosition - transform.root.position).normalized * hit.distance), Color.red, 20);
                if (hit.transform.gameObject.tag == "Player")
                {
                    detected = true;
                    GameObject.Find("Player").GetComponent<FPCharacter>().allowMovement = false;
                    detectedPos = transform.root.position;
                }
            }
        }
    }
}
