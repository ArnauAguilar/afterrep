﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Color32 FadeIn;
    public Color32 FadeOut;
    public float fadeSpeed = 10;
    public static GameController instance;
    public CompanionController companion;
    public FPCharacter player;
    private int sceen = 0;
    public FollowPath[] paths;
    public Vector3 persequeSpot;
    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }
    int sceneToGo;
    public IEnumerator ChangeScene(float t, int i)
    {
        yield return new WaitForSeconds(t);
        endFadeOut = true;
        sceneToGo = i;
    }

    IEnumerator setPlayerPath(float t, bool crouch = false, bool run = false)
    {
        yield return new WaitForSecondsRealtime(t);
        player.animCrouch = crouch;
        player.animRun = run;
        player.paths[0] = paths[1];
        player.paths[0].current = 0;
    }

    public void givePlayerControl(bool t)
    {
        player.allowMovement = t;
        player.gameObject.GetComponent<FPMouseLook>().enabled = t;
       Camera.main.GetComponent<FPMouseLook>().enabled = t;
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private bool gameStarted;
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        gameStarted = false;//StartFunc();
        endFadeOut = false;
        alpha = 1;
    }
    float alpha = 1;
    bool Fade(bool fadeIn)
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            player = GameObject.Find("Player").GetComponent<FPCharacter>();
            companion = GameObject.Find("Friend").GetComponent<CompanionController>();
            givePlayerControl(false);
        }

        alpha += fadeIn ? -Time.deltaTime * fadeSpeed : Time.deltaTime * fadeSpeed;
        GameObject.Find("Fade").GetComponent<Image>().color = new Color(FadeIn.r, FadeIn.g, FadeIn.b, alpha);
        if (fadeIn) return alpha <= 0;
        else return alpha >= 1;
    }
    IEnumerator StartDialogue(float t, string toPlay)
    {
        yield return new WaitForSeconds(t);
        GetComponent<DialogueManager>().PlayDialogue(toPlay);
    }
    void StartFunc()
    {
        Debug.Log("start");
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            player = GameObject.Find("Player").GetComponent<FPCharacter>();
            companion = GameObject.Find("Friend").GetComponent<CompanionController>();
        }
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            paths[0].current = 0;
            paths[1].current = 0;
            paths[2].current = 0;
            givePlayerControl(false);
            player.paths[0] = paths[0];
            player.paths[0].current = 0;
            player.animCrouch = true;
            companion.paths[0] = paths[2];
            companion.action = CompanionController.Action.followPath;
            StartCoroutine(StartDialogue(5.5f, "Dialogue1"));
            Debug.Log("here1");
            StartCoroutine(setPlayerPath(5,false,false));
        }
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            //givePlayerControl(true);
            //companion.action = CompanionController.Action.followPlayer;
            GetComponent<AnimationPlayer>().PlayScene3InitalAnimation();

        }
        //StartCoroutine(givePlayerControl(5));
    }

    public void StartGame()////////////////////////Change to 1
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }
    void Update()
    {
        if (!gameStarted)
        {
            if (Fade(true))
            {
                StartFunc();
                gameStarted = true;
            }
        }
        else
        {
            if (SceneManager.GetActiveScene().buildIndex == 1)
            {
                
                PathUpdate();

                if (companion.life <= 0)
                {
                    EndCinematic();
                    givePlayerControl(false);
                    if (sceen == 0)
                    {
                        player.GetComponentInChildren<GunBehaviour>().UnAim = true;
                        companion.action = CompanionController.Action.wait;
                        fadeSpeed = 0.4f;
                        StartCoroutine(StartDialogue(4, "Dialogue2"));
                        StartCoroutine(lookBack(LookBackTime));//LookBackTime
                        StartCoroutine(ChangeScene(endSceneTime, 2));//endSceneTime
                        step = 0;
                    }
                    sceen++;
                }
            }
        }
        if (endFadeOut)
        {
            if (Fade(false))
            {
                SceneManager.LoadScene(sceneToGo, LoadSceneMode.Single);
                player = GameObject.Find("Player").GetComponent<FPCharacter>();
            }
        }
    }
    bool endFadeOut = false;
    int step = 0;
    bool finalCin = false;

    public float LookBackTime = 20, fleeTime = 18, endSceneTime =60, lowerTime = 20;
    void EndCinematic()
    {
        if (step == 0) if (Fade(false))
            {
                companion._charController.enabled = false;
                player._charController.enabled = false;
                companion.gameObject.transform.position = GameObject.Find("PosCompanion").transform.position;   
                player.gameObject.transform.position = GameObject.Find("PosPlayer").transform.position;
                Camera.main.transform.forward = companion.gameObject.transform.position - player.gameObject.transform.position;
                companion._charController.enabled = true;
                player._charController.enabled = true;

                step = 1;
            }
        if(step == 1)
        {
            if (Fade(true))
            {
                companion.dead = false;
                companion.life = 100;
                companion.anim.SetBool("Dead", false);
                finalCin = true;
                companion.action = CompanionController.Action.wait;

            }
        }

    }

    IEnumerator setCompanionPath(float t, int i)
    {
        yield return new WaitForSecondsRealtime(t);
        companion.paths[0] = paths[i];
        companion.action = CompanionController.Action.followPath;
        Debug.Log("here");
    }
    IEnumerator lower(float t)
    {
        yield return new WaitForSecondsRealtime(t);
        GetComponent<AuxiliarGameControllerScript>().lowerObjects();
    }

    bool lookSceneDesapear = false;
    float lookTimer = 0;
    IEnumerator lookBack(float t)
    {
        yield return new WaitForSecondsRealtime(t);
        lookSceneDesapear = true;
    }
    IEnumerator returnRotation(float t)
    {
        yield return new WaitForSecondsRealtime(t);
        lookSceneDesapear = false;
    }
    bool setedPath = false;
    bool asist = false;
    float timer = 0;
    void PathUpdate()
    {
        //PRESENTATION SCENE
        if (finalCin)
        {
            if (!setedPath)
            {
                StartCoroutine(setCompanionPath(fleeTime, 4));//Tiempo que tarda en irse
                StartCoroutine(lower(lowerTime));
                setedPath = true;
            }
            //StartCoroutine(shoot(6));
            if (lookSceneDesapear)
            {
                if (Camera.main.transform.rotation.eulerAngles.y < 180) Camera.main.transform.Rotate(Vector3.up, 50 * Time.deltaTime);
                else StartCoroutine(returnRotation(2));
                asist = true;
            }
            else if(asist)
            {
                if (Camera.main.transform.rotation.eulerAngles.y > 1f) Camera.main.transform.Rotate(Vector3.up, -80 * Time.deltaTime);
                else
                {
                    asist = false;
                    givePlayerControl(true);
                }
            }
            if(asist == false)
            {
                Vector3 camForward = companion.actualPosition - Camera.main.transform.position;
                camForward.y = 0;
                Camera.main.transform.forward = camForward;
            }
        }
        else
        {
            if (paths[1].finished() && paths[2].finished() && timer >=15)//list here the last pahts that need to be finished in order to start yhe scene
            {
                companion.paths[0] = paths[3];
                companion.action = CompanionController.Action.evil;
                givePlayerControl(true);
            }
            else
            {
                if(paths[2].finished()) companion.action = CompanionController.Action.wait;
                Vector3 camForward = companion.actualPosition - Camera.main.transform.position;
                camForward.y = 0;
                Camera.main.transform.forward = camForward;
            }
        }
        //
        //FIRST LEVEL

        //
        timer += Time.deltaTime;
    }

}
