﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public float rotation = 0;
    public float speed = 10;
    public bool forward = true;
    public Quaternion toRotate;
    public Quaternion startPoint;
    void Start()
    {
        toRotate = transform.rotation;
    }

    public void Rotate(float d)
    {
        float angle = d / 360 * 2 * Mathf.PI;
        Vector3 axis = Vector3.up * (Mathf.Sin(angle / 2));
        float w =  Mathf.Cos(angle / 2);
        toRotate = new Quaternion(axis.x, axis.y, axis.z, w);
        toRotate.Normalize();
        toRotate = toRotate * transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotate, speed * Time.deltaTime);
    }
}
