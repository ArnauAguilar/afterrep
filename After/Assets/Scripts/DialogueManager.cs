﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    public GameObject canvas;
    public GameObject dialoguePrefab;
    public Dialogue[] dialogues;
    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas");
    }
    public void PlayDialogue(string toPlay)
    {
        canvas = GameObject.Find("Canvas");
        foreach (Dialogue d in dialogues)
        {
            if (d.id == toPlay) d.PlayDialogue(0, this, canvas);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
