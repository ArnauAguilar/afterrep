﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkOnFloor : MonoBehaviour
{

    private void OnTriggerStay(Collider other)
    {
        if (other.tag != "player")
        {
            transform.GetComponentInParent<FPCharacter>().OnFloor = true;
        }
    }

}
