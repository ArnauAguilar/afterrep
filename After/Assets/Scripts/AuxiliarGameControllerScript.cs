﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuxiliarGameControllerScript : MonoBehaviour
{
    public List<GameObject> objs;
    private float timer = 0;
    private bool lower = false;
    public float speed;
    public void lowerObjects()
    {
        timer = 0;
        lower = true;
        objs = new List<GameObject>();
        objs.Add(GameObject.Find("Lowerable1"));
        objs.Add(GameObject.Find("Lowerable2"));
        objs.Add(GameObject.Find("Lowerable3"));
    }
    void Update()
    {
        if(lower)
        for(int i = 0; i< objs.Count; i++)
        {
            objs[i].transform.position -= Vector3.up *  Time.deltaTime * speed;
        }
    }
}
