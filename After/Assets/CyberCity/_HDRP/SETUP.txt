Important:
- This folder contains HDRP version of Cyber City as complete project;
- Be careful: complete project may overwrite your project settings;
- Use empty project for import;
- Use Unity 2019.1 or higher (Important: requires Unity 2019.1 or higher).

Install:
- create empty project;
- click Assets > Import Package > Custom Package;
- goto 'HDRP Cyber City 2019.1' file via explorer;
- import.