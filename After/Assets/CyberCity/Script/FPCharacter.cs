﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class FPCharacter : MonoBehaviour
{
	public float speed = 2.0f;
    public float speedCrouch = 0.5f;
    public float speedFast = 10.0f;
	public float gravity = -9.8f;
    float _speedMult;
    public float crouchReduction;
    public float jumpForce;
    private Vector3 height;
    public Vector2 CameraWalkEffect;
    public Vector2 CameraWalkEffectSpeed;
    private Vector2 _CameraWalkEffect;
    private Vector2 _CameraWalkEffectSpeed;
    public CharacterController _charController;
    private Camera mainCam;
    private Vector3 cameraOriginalPos;
    private Vector3 GunOriginalPos;
    [HideInInspector] public bool OnFloor = true;
    private float originalHeight;
    public Vector3 actualPosition;
     public bool allowMovement = true;
    [HideInInspector] public bool animRun = false;
    [HideInInspector] public bool animCrouch = false;
    [HideInInspector] public int pathToFollow = 0;
    public FollowPath[] paths;
    void Start() 
    {
        _charController = GetComponent<CharacterController>();
        originalHeight = _charController.height;
        height = transform.localScale;
        mainCam = Camera.main;
        cameraOriginalPos = mainCam.transform.localPosition;
        GunOriginalPos = mainCam.transform.GetChild(0).localPosition;
        //allowMovement = true;
    }
    public enum state { walking, runing, crouched};
    public state State;
    float resetCameraTimer = 0;
    [HideInInspector] public float deltaX;
    [HideInInspector] public float deltaZ;
    
    float changeTargetCounter = 0;

    private Vector3 _cameraOriginalPos;
    private float jumpThreshold = 0.05f;
    public bool objectOnHand;

    public Vector3 target;
    [HideInInspector] public float _stopDistance = 2;
    void Update() 
    {
        _charController.Move(Vector3.zero);
        actualPosition = transform.position;

        if (allowMovement)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                GameObject.Find("Manager").GetComponent<DialogueManager>().PlayDialogue("Dialogue1");
            }


            _charController.height = originalHeight;
            _cameraOriginalPos = cameraOriginalPos;
            if (Input.GetKey(KeyCode.LeftControl))//crouch
            {
                State = state.crouched;
                _speedMult = speedCrouch;
                _charController.height = originalHeight * crouchReduction;
                _cameraOriginalPos = cameraOriginalPos - Vector3.up * 0.5f;
                //transform.localScale = new Vector3(height.x, height.y * crouchReduction, height.z);
                _CameraWalkEffect = CameraWalkEffect * new Vector2(2,2);
                _CameraWalkEffectSpeed = CameraWalkEffectSpeed * new Vector2(4, 4);
            }
            else if (Input.GetKey(KeyCode.LeftShift))//run
            {
                State = state.runing;
                _speedMult = speedFast;
                transform.localScale = height;
                _CameraWalkEffect = CameraWalkEffect * new Vector2(1, 1);
                _CameraWalkEffectSpeed = CameraWalkEffectSpeed * new Vector2(0.5f, 0.5f);
            }
            else
            {
                State = state.walking;
                _speedMult = 1;
                transform.localScale = height;
                _CameraWalkEffect = CameraWalkEffect * new Vector2(1, 1);
                _CameraWalkEffectSpeed = CameraWalkEffectSpeed * new Vector2(1, 1);
            }



            deltaX = Input.GetAxis("Horizontal") * speed * _speedMult;
            deltaZ = Input.GetAxis("Vertical") * speed * _speedMult;
		    Vector3 movement = new Vector3(deltaX, 0, deltaZ);
            movement = Vector3.ClampMagnitude(movement, speedFast);
		    movement.y = GetComponent<Rigidbody>().velocity.y;
            movement *= Time.deltaTime;
		    movement = transform.TransformDirection(movement);
		    _charController.Move(movement);

            actualPosition = transform.position;


            if (Input.GetKeyDown(KeyCode.Space) && OnFloor)
            {
                OnFloor = false;
                GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, jumpForce, GetComponent<Rigidbody>().velocity.z);
            }

            //camera walk effect      
            if(deltaX != 0 || deltaZ != 0)
            {
                mainCam.transform.localPosition = _cameraOriginalPos + new Vector3(Mathf.Sin(Time.time * CameraWalkEffectSpeed.x) * CameraWalkEffect.x, Mathf.Cos(Time.time * CameraWalkEffectSpeed.y) * CameraWalkEffect.y, 0);
                resetCameraTimer = 0;
            }
            else
            {
                mainCam.transform.localPosition = Vector3.Lerp(mainCam.transform.localPosition, _cameraOriginalPos, Mathf.Clamp01(resetCameraTimer));
                resetCameraTimer += Time.deltaTime;
            }
            //
        }
        else
        {
            _charController.height = originalHeight;
            _cameraOriginalPos = cameraOriginalPos;
            if (animCrouch)//crouch
            {
                State = state.crouched;
                _speedMult = speedCrouch;
                _charController.height = originalHeight * crouchReduction;
                _cameraOriginalPos = cameraOriginalPos - Vector3.up * 0.5f;
                //transform.localScale = new Vector3(height.x, height.y * crouchReduction, height.z);
                _CameraWalkEffect = CameraWalkEffect * new Vector2(2, 2);
                _CameraWalkEffectSpeed = CameraWalkEffectSpeed * new Vector2(4, 4);
            }
            else if (animRun)//run
            {
                State = state.runing;
                _speedMult = speedFast;
                transform.localScale = height;
                _CameraWalkEffect = CameraWalkEffect * new Vector2(1, 1);
                _CameraWalkEffectSpeed = CameraWalkEffectSpeed * new Vector2(0.5f, 0.5f);
            }
            else
            {
                State = state.walking;
                _speedMult = 1;
                transform.localScale = height;
                _CameraWalkEffect = CameraWalkEffect * new Vector2(1, 1);
                _CameraWalkEffectSpeed = CameraWalkEffectSpeed * new Vector2(1, 1);
            }
            /// AutoMove
            target = Vector3.zero;
            
            if (!paths[pathToFollow].finished())
            {
                Vector3 nextPos = new Vector3(paths[pathToFollow].getNextPoint().transform.position.x, actualPosition.y, paths[pathToFollow].getNextPoint().transform.position.z);
                if (Vector3.Distance(actualPosition, nextPos) < _stopDistance)
                {

                    paths[pathToFollow].goToNext();
                    changeTargetCounter = 0;
                }
                target = paths[pathToFollow].getNextPoint().transform.position;
                changeTargetCounter += Time.deltaTime;
            }
            else
            {

                target = actualPosition;
            }


            target = new Vector3(target.x, actualPosition.y, target.z);

            Vector3 movement = target - actualPosition;
            movement.Normalize();
            movement *= _speedMult * speed;
            movement = Vector3.ClampMagnitude(movement, speedFast);
            movement.y = GetComponent<Rigidbody>().velocity.y;
            movement *= Time.deltaTime;
            movement = transform.TransformDirection(movement);
            //camera walk effect      
            if (movement.x != 0 && movement.z != 0)
            {
                mainCam.transform.localPosition = _cameraOriginalPos + new Vector3(Mathf.Sin(Time.time * CameraWalkEffectSpeed.x) * CameraWalkEffect.x, Mathf.Cos(Time.time * CameraWalkEffectSpeed.y) * CameraWalkEffect.y, 0);
                mainCam.transform.GetChild(0).localPosition = GunOriginalPos + new Vector3(-Mathf.Sin(Time.time * CameraWalkEffectSpeed.x) * CameraWalkEffect.x / 3, -Mathf.Cos(Time.time * CameraWalkEffectSpeed.y) * CameraWalkEffect.y / 3, 0);
                resetCameraTimer = 0;
            }
            else
            {
                mainCam.transform.localPosition = Vector3.Lerp(mainCam.transform.localPosition, _cameraOriginalPos, Mathf.Clamp01(resetCameraTimer));
                resetCameraTimer += Time.deltaTime;
            }
            //

            _charController.Move(movement);
        }
    }

    public float pushPower = 2.0f;
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //if (hit.collider.gameObject.tag == "Ground") OnFloor = true;


        Rigidbody body = hit.collider.attachedRigidbody;
        if (body == null || body.isKinematic)
        {
            return;
        }

        // We dont want to push objects below us
        if (hit.moveDirection.y < -0.3)
        {
            return;
        }

        // Calculate push direction from move direction,
        // we only push objects to the sides never up and down
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        // If you know how fast your character is trying to move,
        // then you can also multiply the push velocity by that.

        // Apply the push
        body.velocity = pushDir * pushPower;
    }
    IEnumerator ResetJump()
    {
        yield return new WaitForEndOfFrame();
        OnFloor = false;
    }
}
