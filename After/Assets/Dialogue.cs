﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Dialogue", menuName = "ScriptableObjects/Dialogue", order = 1)]
public class Dialogue : ScriptableObject
{
    public string id;
    public string[] texts;
    public string[] talker;
    public AudioClip[] voices;
    public float[] offsets;

    IEnumerator PlayNext(int i, float t, DialogueManager manager, GameObject canvas)
    {
        yield return new WaitForSeconds(t + offsets[i]);
        PlayDialogue(++i, manager, canvas);
    }

    public void PlayDialogue(int i, DialogueManager manager, GameObject canvas)
    {
        AudioSource source = GameObject.Find(talker[i]).GetComponent<AudioSource>() == null ? GameObject.Find(talker[i]).AddComponent<AudioSource>() : GameObject.Find(talker[i]).GetComponent<AudioSource>();
        source.clip = voices[i];
        source.maxDistance = 40;
        source.dopplerLevel = 0.12f;
        if (!source.gameObject.transform.GetComponentInChildren<AudioListener>()) source.spatialBlend = 0.5f;
        else source.spatialBlend = 0f;
        source.Play();
        GameObject text = Instantiate(manager.dialoguePrefab, canvas.transform);
        text.GetComponent<TextMeshProUGUI>().text = texts[i];
        Destroy(text, source.clip.length);

        if (i < texts.Length - 1) manager.StartCoroutine(PlayNext(i, source.clip.length, manager, canvas));
    }
}

