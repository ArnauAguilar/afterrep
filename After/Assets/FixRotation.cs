﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixRotation : MonoBehaviour
{
    Quaternion original;
    void Start()
    {
        original = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = original;
    }
}
