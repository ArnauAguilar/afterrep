﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pushable : MonoBehaviour
{
    public float detectionDistance = 2;
    public GameObject Text;
    private GameObject insText;
    public float fallForce;

    // Update is called once per frame
    private void Start()
    {
        transform.GetComponentInChildren<Rigidbody>().isKinematic = true;
    }
    IEnumerator setKinematic(float t)
    {
        yield return new WaitForSeconds(t);
        transform.GetComponentInChildren<Rigidbody>().isKinematic = true;
        fallen = true;
    }
    public bool pushed;
    public bool fallen;
    void Update()
    {

        if (pushed)
        {
            Destroy(insText);
            return;
        }
        if(Vector3.Distance(GameController.instance.player.actualPosition, transform.position) < detectionDistance && insText == null)
        {
            insText = Instantiate(Text, GameObject.Find("Canvas").transform);
            insText.GetComponent<TextMeshProUGUI>().SetText("F");
        }


        if (Input.GetKeyDown(KeyCode.F) && Vector3.Distance(GameController.instance.player.actualPosition, transform.position) < detectionDistance && insText != null)
        {
            transform.GetComponentInChildren<Rigidbody>().isKinematic = false;
            transform.GetComponentInChildren<Rigidbody>().AddForce((transform.position - GameController.instance.player.actualPosition).normalized * fallForce);
            pushed = true;
            StartCoroutine(setKinematic(2.5f));
        }

    }
}
