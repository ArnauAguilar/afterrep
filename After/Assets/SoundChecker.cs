﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundChecker : MonoBehaviour
{
    // Start is called before the first frame update
    Pushable pushableObj;
    public float movementDpeed;

    void Start()
    {
        pushableObj = FindObjectsOfType<Pushable>()[0];
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pushableObj.fallen)
        {
            transform.GetChild(1).GetComponent<GuardBehaviour>().enabled = false;
            Vector3 center = pushableObj.transform.GetChild(0).GetComponent<Renderer>().bounds.center;
            Vector3 targget = center + ((transform.root.position - center).normalized * 1.5f);
            targget.y = transform.root.position.y;

            Vector3 f = center - transform.root.position;
            f.y = 0;

            transform.root.position = Vector3.MoveTowards(transform.root.position, targget, movementDpeed * Time.deltaTime);
            transform.root.forward = f;
        }
    }
}
