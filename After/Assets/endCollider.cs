﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameController.instance.StartCoroutine(GameController.instance.ChangeScene(1, 0));
        }
    }
}
