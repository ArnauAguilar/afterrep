﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotable : MonoBehaviour
{
    public bool shooted;
    public float times;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            shooted = true;
            times++;
        }
    }
}
