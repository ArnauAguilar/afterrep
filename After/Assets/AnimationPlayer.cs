﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class AnimationPlayer : MonoBehaviour
{
    public CompanionController companion;
    public FPCharacter player;
    bool lookFriend = false;
    public Transform head;
    private Vector3 originalForward;

    bool acting, FreindLookDrone, lookDrone, lookAnn = true;

    public void PlayScene3InitalAnimation()
    {
        originalForward = Camera.main.transform.forward;
        acting = true;
        player = GameController.instance.player;
        companion = GameController.instance.companion;
        head = companion.transform.GetChild(0).GetChild(1).GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetChild(0);
        player._stopDistance = 0.8f;
        companion.stopDistance = 0.2f;
        companion.action = CompanionController.Action.followPath;
        GameController.instance.givePlayerControl(false);
        fov = Camera.main.fieldOfView;
        Debug.Log("onlyOnce");
        //first part of animation
        player.paths[0] = GameController.instance.paths[6];
        player.paths[0].current = 0;
        companion.paths[0] = GameController.instance.paths[7];
        companion.paths[0].current = 0;
        StartCoroutine(LookFriendIn(0.3f));
        StartCoroutine(playDialogue(0.5f));
        StartCoroutine(friendLookAnn(0.2f));
        StartCoroutine(friendLookDrone(6f));
        StartCoroutine(crouchEm(7.3f));
        StartCoroutine(LookDroneIn(7.8f));
        StartCoroutine(LookFriendIn(11.8f));
        StartCoroutine(friendLookAnn(11.8f));
        StartCoroutine(friendLookDrone(25f));
        StartCoroutine(LookDroneIn(26f, true));
        StartCoroutine(SetFovToGo(31f, false));
        StartCoroutine(friendLookAnn(34f));
        StartCoroutine(LookFriendIn(35f));
        StartCoroutine(Correction(49f));
        StartCoroutine(ReturnControl(51f));

    }
    bool correction = false;
    float correctionT = 0;
    IEnumerator Correction(float t)
    {
        yield return new WaitForSeconds(t);
        correction = true;
        correctionT = 0;
    }
    IEnumerator ReturnControl(float t)
    { 
        yield return new WaitForSeconds(t);
        companion.action = CompanionController.Action.followPlayer;
        GameController.instance.givePlayerControl(true);
        acting = false;
        player._stopDistance = 2;
        companion.stopDistance = 4f;
    }
    IEnumerator LookFriendIn(float t)
    {
        yield return new WaitForSeconds(t);
        lookDrone = false;
        lookFriend = true;
        timer = 0;
    }
    bool zoom;
    IEnumerator SetFovToGo(float t = 1,bool b = true)
    {
        yield return new WaitForSeconds(t);
        zoom = b;
    }
    IEnumerator LookDroneIn(float t, bool changeFov = false)
    {
        yield return new WaitForSeconds(t);
        if (changeFov) StartCoroutine(SetFovToGo());
        lookFriend = false;
        lookDrone = true;
        droneTimer = 0;
    }

    IEnumerator friendLookDrone(float t)
    {
        yield return new WaitForSeconds(t);
        FreindLookDrone = true;
        lookAnn = false;
        headTimer = 0;
    }

    IEnumerator friendLookAnn(float t)
    {
        yield return new WaitForSeconds(t);
        FreindLookDrone = false;
        lookAnn = true;
        playerHeadTime = 0;
    }

    IEnumerator crouchEm(float t)
    {
        yield return new WaitForSeconds(t);
        player.animCrouch = true;
        player.animRun = false;
        companion.animState = FPCharacter.state.crouched;
    }


    IEnumerator playDialogue(float t)
    {
        yield return new WaitForSeconds(t);
        GetComponent<DialogueManager>().PlayDialogue("Dialogue3");
    }

    float timer, headTimer, droneTimer, playerHeadTime;
    float slowFactor = 15;
    float fov;
    public void Update()
    {
       
    }

    public void LateUpdate()
    {

        if (acting)
        {
            if (!zoom && Camera.main.fieldOfView < fov)
            {
                Camera.main.fieldOfView += Time.deltaTime * 30;
                if (Camera.main.fieldOfView > fov) Camera.main.fieldOfView = fov;
            }
            if (companion.paths[0].finished()) companion.action = CompanionController.Action.wait;
            if (lookDrone)
            {
                if (zoom && Camera.main.fieldOfView > 22) Camera.main.fieldOfView -= Time.deltaTime * 60;
                Vector3 camForward = GameObject.Find("AnimationDrone").transform.position - Camera.main.transform.position;
                Camera.main.transform.forward = Vector3.Lerp(Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.position + camForward.normalized, droneTimer / slowFactor) - Camera.main.transform.position ;
                droneTimer += Time.deltaTime;
            }
            else if (lookFriend)
            {
                Vector3 camForward = companion.actualPosition - Camera.main.transform.position;
                camForward.y = 0;
                Camera.main.transform.forward = Vector3.Lerp(Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.position + camForward, timer / 15) - Camera.main.transform.position;
                timer += Time.deltaTime;
            }
            if (FreindLookDrone)
            {
               
                Vector3 droneDir = GameObject.Find("AnimationDrone").transform.position - head.transform.position;
                

                Vector3 forward = Vector3.Lerp(head.transform.position + head.transform.forward, head.transform.position + droneDir.normalized, headTimer / 2) - head.transform.position;
                if(Vector3.Angle(forward, player.transform.position - companion.transform.position) < 50)
                {
                    headTimer += Time.deltaTime;
                }
                head.transform.forward = forward + Vector3.up/4;
            }
            else if(lookAnn)
            {
                //Debug.Log("LookAnn "+ playerHeadTime/2);
                Vector3 playerDir = player.transform.position - head.transform.position;

                Vector3 forward = Vector3.Lerp(head.transform.position + head.transform.forward, head.transform.position + playerDir.normalized, playerHeadTime / 2) - head.transform.position;
                if (Vector3.Angle(forward, player.transform.position - companion.transform.position) < 180)
                {
                    playerHeadTime += Time.deltaTime;
                }

                head.transform.forward = forward + Vector3.up / 4;
            }
            if (correction)
            {
                Camera.main.transform.forward = Vector3.Lerp(Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.position + originalForward, correctionT ) - Camera.main.transform.position;
                correctionT +=Time.deltaTime;

            }
        }
    }
}
